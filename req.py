#!/opt/rh/python27/root/usr/bin/python
# -*- coding: UTF-8 -*- #


__version__ = "0.0.9"
__author__ = "altynos@gmail.com"


import suds
import platform
import os
import zipfile
import logging
import sys
import arrow
import re
import subprocess
import logging
import sys
import time
import hashlib
import smtplib
import ConfigParser
import urllib
from shutil import copyfile
from base64 import b64encode, b64decode
from lxml import etree
from datetime import datetime
from ipaddr import IPv4Network
from logging.handlers import RotatingFileHandler
from urlparse import urlparse, urlunparse
from berserker_resolver import Resolver

config = ConfigParser.RawConfigParser()
config.read('rkn.cfg')

requestFile = config.get('Sign', 'name_request')
signatureFile = config.get('Sign', 'name_signedreq')

dir = os.path.dirname(__file__)
TS = arrow.now().format('YYYY-MM-DDTHH-mm-ss')



logger = logging.getLogger(__name__)
file_handler = RotatingFileHandler(config.get('Log', 'logfile'), 'a', 1 * 1024 * 1024, 10)
file_handler.setFormatter(logging.Formatter('%(asctime)s %(levelname)s: %(message)s'))
logger.setLevel(logging.INFO)
file_handler.setLevel(logging.INFO)
logger.addHandler(file_handler)

ZALET = []

class ZapretInfoException(RuntimeError):
    pass
class SMTPException(RuntimeError):
    pass

class ZapretInfo(object):
    def __init__(self):
        self.cl = suds.client.Client(config.get('WSDL', 'prod'))

    def getLastDumpDateEx(self):
        '''
        Получение временной метки последнего обновления выгрузки из реестра,
        а также получение информации о версиях веб-сервиса, памятки и текущего формата выгрузки.
        '''
        result = self.cl.service.getLastDumpDateEx()
        return result


    def sendRequest(self, privatekeyFile, x509certfile, versionNum='2.2'):
        '''
        Отправка запроса на получение выгрузки из реестра.
        '''

        if platform.system() == 'Linux':
            cert_txt = subprocess.check_output(["/opt/openssl-gost/usr/bin/openssl", "x509", "-text", "-noout", "-in", x509certfile, "-nameopt", "utf8"])
        else:
            cert_txt = subprocess.check_output(["openssl", "x509", "-text", "-noout", "-in", config.get('Crypto', 'x509cert'), "-nameopt", "utf8"])
        cn_r = re.compile(r'CN=(?P<cn>.*")')
        cn = cn_r.findall(cert_txt)
        ogrn_r = re.compile(r'1\.2\.643\.100\.1=(?P<ogrn>[0-9]{13})')
        ogrn = ogrn_r.findall(cert_txt)
        inn_r = re.compile(r'1\.2\.643\.3\.131\.1\.1=(?P<inn>[0-9]{12})')
        inn = inn_r.findall(cert_txt)
        email_r = re.compile(r'emailAddress=(?P<email>\b[\w\.-]+@[\w\.-]+\.\w{2,4}\b)')
        email = email_r.findall(cert_txt)
        val_period = re.compile(r'(?P<word>\b\w+\b)')
 
        req = etree.XML(
            '<request>'
            '</request>')
        etree.SubElement(req, "requestTime").text = arrow.now().format('YYYY-MM-DDTHH:mm:ss.SSSZZ')
        etree.SubElement(req, "operatorName").text = cn[1].decode('utf8')
        etree.SubElement(req, "inn").text = inn[1]
        etree.SubElement(req, "ogrn").text = ogrn[1]
        etree.SubElement(req, "email").text = email[1]

        doc = etree.ElementTree(req)

        outFile = open(requestFile, 'w')
        outFile.write('<?xml version="1.0" encoding="windows-1251"?>')
        doc.write(outFile, xml_declaration=False, encoding='windows-1251', pretty_print=True)
        outFile.close()
        if platform.system() == 'Linux':
            result_sign = subprocess.check_output(["/opt/openssl-gost/usr/bin/openssl", "smime", "-sign", "-in", "request.xml", "-out", signatureFile, "-signer", privatekeyFile, "-engine", "gost", "-gost89", "-binary", "-noattr", "-outform", "DER" ])
        else:
            result_sign = subprocess.check_output(["openssl", "smime", "-sign", "-in", "request.xml", "-out", signatureFile, "-signer", config.get('Crypto', 'privkey'), "-outform", "DER" ])
        os.makedirs(os.path.join(dir,'archive', 'requests', TS))
        copyfile('request.xml', os.path.join(dir,'archive', 'requests', TS, 'request.xml'))
        copyfile('request.xml.sign', os.path.join(dir,'archive', 'requests', TS, 'request.xml.sign'))


        if not os.path.exists(requestFile):
            raise ZapretInfoException('No request file')
        if not os.path.exists(signatureFile):
            raise ZapretInfoException('No signature file')

        with open(requestFile, "rb") as f:
            data = f.read()

        xml = b64encode(data)

        with open(signatureFile, "rb") as f:
            data = f.readlines()

        if '-----' in data[0]:
            sert = ''.join(data[1:-1])
        else:
            sert = ''.join(data)

        sert = b64encode(sert)
        result = self.cl.service.sendRequest(xml, sert, versionNum)

        return dict(((k, v.encode('utf-8')) if isinstance(v, suds.sax.text.Text) else (k, v)) for (k, v) in result)

    def getResult(self, code):
        '''
        Получение результата обработки запроса - выгрузки из реестра
        '''
        result = self.cl.service.getResult(code)

        return dict(((k, v.encode('utf-8')) if isinstance(v, suds.sax.text.Text) else (k, v)) for (k, v) in result)


def send_email(message, logger):
    '''Отправка почты'''
    try:
        smtpObj = smtplib.SMTP(config.get('Mail', 'server'))
        smtpObj.sendmail(config.get('Mail', 'server'), config.get('Mail', 'recipients').split(','), message)         
        logger.info('Successfully sent email')
    except:
        logger.info('Error: unable to send email')

def sort_ip_list(ips):
    """Сортировка ip адресов"""
    for i in range(len(ips)):
        ips[i] = "%3s.%3s.%3s.%3s" % tuple(ips[i].split("."))
        ips.sort()
    for i in range(len(ips)):
        ips[i] = ips[i].replace(" ", "")
    return ips


def urlEncodeNonAscii(b):
    '''Кодируем non ascii'''
    return re.sub('[\x80-\xFF]', lambda c: '%%%02x' % ord(c.group(0)), b)



def iri_to_uri(iri):
    '''Конвертация из iri to uri'''
    parts= urlparse(iri)
    return urlunparse(
        part.encode('idna') if parti==1 else urlEncodeNonAscii(part.encode('utf-8'))
        for parti, part in enumerate(parts))


def check_updates(session, logger):
    '''Проверяем наличие обновлений'''
    logger.info('Check if dump.xml already exists.')
    if os.path.exists('dump.xml'):
        logger.info('dump.xml already exists.')
        
        data = etree.parse('dump.xml')
        dt = datetime.strptime(data.xpath('./@updateTime')[0][:-6], '%Y-%m-%dT%H:%M:%S')
        updateTime = int(time.mktime(dt.timetuple()))
        logger.info('Got updateTime: %s.', updateTime)

        dt = datetime.strptime(
            data.xpath('./@updateTimeUrgently')[0][:-6], '%Y-%m-%dT%H:%M:%S')
        updateTimeUrgently = int(time.mktime(dt.timetuple()))
        logger.info('Got updateTimeUrgently: %s.', updateTimeUrgently)

        fromFile = max(updateTime, updateTimeUrgently)
        logger.info('Got latest update time: %s.', fromFile)
    else:
        logger.info('dump.xml does not exist')
        fromFile = 0

    logger.info('Check if dump.xml has updates since last sync.')
    last_dump = session.getLastDumpDateEx()
    logger.info('Current versions: webservice: %s, dump: %s, doc: %s',
                last_dump.webServiceVersion,
                last_dump.dumpFormatVersion,
                last_dump.docVersion)
    if max(last_dump.lastDumpDate, last_dump.lastDumpDateUrgently) / \
            1000 != fromFile:
        logger.info('New dump is available.')
        return True
    else:
        return False

def resolve_domains(domains):
    '''Резолвим домены'''
    ips = []
    int_domains = [item.encode('idna') for item in domains]
    resolver = Resolver(www=True, www_combine=True, nameservers=['8.8.8.8'], tries=1)
    result = resolver.resolve(int_domains)
    for key in result:
        for item in result[key]:
            ips.append(str(item))
    return ips


def resolve_helper_url(urls):
    '''Хелпер для редиректа url'''
    filtered_domains = []
    for url in urls:
        url = urlparse(url)
        filtered_domains.append(url.netloc.encode('idna'))
    return filtered_domains


def parse_dump(filename ,ZALET, logger):
    '''Парсим дамп и формируем acls'''
    if not os.path.exists(os.path.join(dir,'acl')):
        os.makedirs(os.path.join(dir,'acl'))
    dump = etree.parse(filename)
    reject_ips = dump.xpath("//content[@blockType='ip']/ip/text()")
    urls = sorted(list(set(dump.xpath("//content/url/text()"))))
    redirect_ips = dump.xpath("//content/ip/text()")
    subnets = dump.xpath("//content/ipSubnet/text()")
    block_domains = sorted(list(set(dump.xpath("//content[@blockType='domain']/domain/text()"))))
    list_for_resolve = list(set(block_domains + list(set(resolve_helper_url(urls)))))
    logger.info('Starting resolve')
    redirect_ips = list(set(redirect_ips + resolve_domains(list_for_resolve)))
    logger.info('Complete resolve '+str(len(redirect_ips))+' ips')
    

    with open(os.path.join(dir,'acl','block_ip_all_borovaya'), "w") as f:
        for item in sort_ip_list(reject_ips):
            if ip_in_whitelist(item):
                ZALET.append(item)
            f.write("route %s/32 reject;\n" % item)
    with open(os.path.join(dir,'acl','block_ip_all_korova'), "w") as f:
        for item in sort_ip_list(reject_ips):
            f.write("route %s/32 reject;\n" % item)
    with open(os.path.join(dir,'acl','subnets'), "w") as f:
        for item in subnets:
            f.write("%s\n" % item)
    with open(os.path.join(dir,'acl','block_domain_all'), "w") as f:
        for item in block_domains:
            f.write("%s\n" % item.encode('idna'))
    with open(os.path.join(dir,'acl','redirect_ips_borovaya'), "w") as f:
        for item in sort_ip_list(redirect_ips):
            if ip_in_whitelist(item):
                ZALET.append(item)
            f.write("route %s/32 reject;\n" % item)
    with open(os.path.join(dir,'acl','redirect_ips_korova'), "w") as f:
        for item in sort_ip_list(redirect_ips):
            f.write("route %s/32 reject;\n" % item)
    with open(os.path.join(dir,'acl','block_urls'), "w") as f: 
        for item in urls:
            f.write("%s\n" % iri_to_uri(item))
    logger.info('Do we need an email with our blocked ip?')

    if ZALET:
        logger.info('Yes!!!')
        print ZALET

def ip_in_whitelist(request_ip):
    '''Проверяем свои адреса''' 
    user_ip = IPv4Network(request_ip).ip
    
    for whitelist_ip in map(str.strip, config.get('Network', 'NW').split(',')):
        w_ip = IPv4Network(whitelist_ip)
        if (user_ip == w_ip.network) or ((user_ip >= w_ip.network) and (user_ip < w_ip.broadcast)):
            return True
    return False


def send_request(session): 
    '''Отправка запроса'''       
    request = session.sendRequest(config.get('Crypto', 'privkey'), config.get('Crypto', 'x509cert'), '2.2')
    logger.info('Checking request status.')
    if request['result']:
        code = request['code']
        logger.info('Got code %s', code)
        logger.info('Waiting for a minute...')
        try:
            os.remove('dump.xml')
        except:
            pass
        time.sleep(60)
        while True:
            logger.info('Trying to get result...')
            request = session.getResult(code)
            if request['result']:
                logger.info('Got a dump ver. %s for the %s (INN %s)',
                            request['dumpFormatVersion'],
                            request['operatorName'].decode('utf-8'),
                            request['inn'])
                with open('result.zip', "wb") as f:
                    f.write(b64decode(request['registerZipArchive']))
                    f.flush()
                    rep = 'Downloaded dump %d bytes, MD5 hashsum: %s',
                    os.path.getsize('result.zip'),
                    hashlib.md5(
                        open(
                            'result.zip',
                            'rb').read()).hexdigest()
                    logger.info(
                    'Downloaded dump %d bytes, MD5 hashsum: %s',
                    os.path.getsize('result.zip'),
                    hashlib.md5(
                        open(
                            'result.zip',
                            'rb').read()).hexdigest())
                    send_email('Downloaded DUMP!', logger)
                try:
                    logger.info('Unpacking.')
                    zip_file = zipfile.ZipFile('result.zip', 'r')
                    with open('dump.xml', "w") as g:
                        g.write(zip_file.read('dump.xml'))
                        g.flush()
                    zip_file.extractall(
                        '%s/%s' %
                        ('archive/dumps', TS))
                    zip_file.close()
                except zipfile.BadZipfile:
                    logger.error('Wrong file format.')
                logger.info('Trying to parse dump...')
                parse_dump("dump.xml", ZALET, logger)
                logger.info("Trying start Sorokin's script")
                try:
                    subprocess.call("/opt/req/squid_reload.sh", shell=True)
                except:
                    logger.info("Sorokin's script not started")
                break
                    
            else:
                if request['resultCode'] == 0:
                    logger.info('Not ready yet. Waiting for a minute.')
                    time.sleep(60)
                else:
                    logger.error('Got an error, code %d: %s',
                                request['resultCode'],
                                request['resultComment'].decode('utf-8'))
                    break
        else:
            logger.error(request['resultComment'].decode('utf-8'))



def main():
    cutoff = time.time() - 86000
    logger.info('Starting script.')
    session = ZapretInfo()
    if check_updates(session, logger):
        logger.info('New dump is available.')
        logger.info('Sending request.')
        send_request(session)
    else:
        if os.path.getctime('dump.xml') < cutoff:
            logger.info('Old dump')
            send_request(session)
        logger.info('No updates.')
    logger.info('Script stopped.')

if __name__ == '__main__':
    main()